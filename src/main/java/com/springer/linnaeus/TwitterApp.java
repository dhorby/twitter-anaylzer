package com.springer.linnaeus;

import com.springer.linnaeus.controller.IndexController;
import com.springer.linnaeus.twitter.TwitterConnect;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import twitter4j.TwitterException;

import javax.annotation.PostConstruct;
import javax.jms.JMSException;
import java.io.IOException;
import java.util.Arrays;

@SpringBootApplication
@EnableScheduling
public class TwitterApp {

    public static void main(String[] args) {

        ApplicationContext ctx = SpringApplication.run(TwitterApp.class, args);

        SparkConf sparkConf = new SparkConf().setAppName("TwitterAnalyzer").setMaster("local[2]").set("spark.executor.memory", "1g");
        SparkContext sc = new SparkContext(sparkConf);

    }


    @PostConstruct
    private static void monitorTwitter() {
        System.out.println("Started");
        try {
            TwitterConnect.checkStream();
        } catch (TwitterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Finished");
    }
}

