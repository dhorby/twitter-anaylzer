package com.springer.linnaeus.service;

import com.springer.linnaeus.controller.Greeting;
import com.springer.linnaeus.twitter.TwitterConnect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.core.MessageSendingOperations;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Component
public class GreetingService {

    private final MessageSendingOperations<String> messagingTemplate;

    @Autowired
    public GreetingService(
            final MessageSendingOperations<String> messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }

    @Scheduled(fixedDelay=1000)
    public void sendQuotes() {

         String destination = "/topic/tweets" ;
         String messageStr = "Hello World!";
         if (TwitterConnect.twitterList.peek() != null) {
             messageStr = TwitterConnect.twitterList.poll().toString();
             this.messagingTemplate.convertAndSend(destination, new Greeting(1, messageStr));
         }
        String twitterWordsDest = "/topic/tweetwords" ;
        if (TwitterConnect.twitterWordList.peek() != null) {

            String twitterWords = TwitterConnect.twitterWordList.poll().toString();
            System.out.println("++++" + twitterWords);
            this.messagingTemplate.convertAndSend(twitterWordsDest, new Greeting(1, twitterWords));
        }
    }

}