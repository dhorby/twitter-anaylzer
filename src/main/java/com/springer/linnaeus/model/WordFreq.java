package com.springer.linnaeus.model;

/**
 * Created by davidhorby on 21/08/15.
 */
public class WordFreq {


    public WordFreq(String word, Long freq) {
        this.word=word;
        this.freq = freq;
    }

    private String word;
    private Long freq;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
