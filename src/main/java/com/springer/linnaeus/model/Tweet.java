package com.springer.linnaeus.model;


public class Tweet {

    private String text;

    public Tweet(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }


}
