package com.springer.linnaeus.twitter;

import java.io.*;
import java.net.*;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.jms.*;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import java.util.*;
import java.util.Queue;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.springer.linnaeus.analyzer.LatentSemanticAnalyzer;
import com.springer.linnaeus.model.Tweet;
import com.springer.linnaeus.model.WordFreq;
import org.apache.activemq.ActiveMQConnectionFactory;

import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.TransportConnector;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.commons.collections.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import twitter4j.*;

public class TwitterConnect {

    public static Queue twitterList = new LinkedList();
    public static Queue twitterWordList = new LinkedList();
    public static Queue wordListQueue = new LinkedList();

    public static final int MAX_TWEETS = 1000;
    public static int counter = 0;

    public static final List<String> stopWords = Arrays.asList(
            "a", "an", "and", "are", "as", "at", "be", "but", "by",
            "for", "if", "in", "into", "is", "it",
            "no", "not", "of", "on", "or", "such",
            "that", "the", "their", "then", "there", "these",
            "they", "this", "to", "was", "will", "with", "we", "were", "has", "between", "from", "using", "have", "we", "which", "null", "most", "one", "more", "1", "p",
            "used", "compared", "can", "two", "high", "than", "other", "our", "well", "been", "during", "found", "both",
            "from",  "have", "associated", "may", "between", "time", "all", "however", "after", "background", "study",
            "also", "had", "using", "g", "its", "under", "important", "significant", "based", "methods", "results", "studies", "use",
            "non", "expression", "different", "significantly", "only", "null", "batch", "growth", "type", "levels", "increased",
            "subjects", "rating", "total", "scores", "scale", "areas", "site", "set","sets", "activity", "left", "right", "vs",
            "nm", "among", "who", "underwent", "scores", "items", "self", "expressed", "expression", "us", "per", "costs", "based", "va",
            "show", "within", "qtl", "snp", "recurrence", "showed", "case", "cases",
            "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "e.g", "i.e", "30cm",
            "type", "here", "further", "could", "would", "sec", "st", "data", "identified", "years", "specific", "related", "including", "first", "three",
            "higher", "analysis", "present", "number", "production", "ci", "0,0", "recent", "ra", "anti",
            "3d","7bit","a0","about","above","abstract","across","additional","after", "conclusions",
            "afterwards","again","against","align","all","almost","alone","along",
            "already","also","although","always","am","among","amongst","amoungst",
            "amount","an","and","another","any","anybody","anyhow","anyone","anything",
            "anyway","anywhere","are","arial","around","as","ascii","assert","at",
            "back","background","base64","bcc","be","became","because","become","becomes",
            "becoming","been","before","beforehand","behind","being","below","beside",
            "besides","between","beyond","bgcolor","blank","blockquote","body","boolean",
            "border","both","br","break","but","by","can","cannot","cant","case","catch",
            "cc","cellpadding","cellspacing","center","char","charset","cheers","class",
            "co","color","colspan","com","con","const","continue","could","couldnt",
            "cry","css","de","dear","default","did","didnt","different","div","do",
            "does","doesnt","done","dont","double","down","due","during","each","eg",
            "eight","either","else","elsewhere","empty","encoding","enough","enum",
            "etc","eu","even","ever","every","everyone","everything","everywhere",
            "except","extends","face","family","few","ffffff","final","finally","float",
            "font","for","former","formerly","fri","from","further","get","give","go",
            "good","got","goto","gt","h1","ha","had","has","hasnt","have","he","head",
            "height","hello","helvetica","hence","her","here","hereafter","hereby",
            "herein","hereupon","hers","herself","hi","him","himself","his","how",
            "however","hr","href","html","http","https","id","ie","if","ill","im",
            "image","img","implements","import","in","inc","instanceof","int","interface",
            "into","is","isnt","iso-8859-1","it","its","itself","ive","just","keep",
            "last","latter","latterly","least","left","less","li","like","long","look",
            "lt","ltd","mail","mailto","many","margin","may","me","meanwhile","message",
            "meta","might","mill","mine","mon","more","moreover","most","mostly","mshtml",
            "mso","much","must","my","myself","name","namely","native","nbsp","need",
            "neither","never","nevertheless","new","next","nine","no","nobody","none",
            "noone","nor","not","nothing","now","nowhere","null","of","off","often",
            "ok","on","once","only","onto","or","org","other","others","otherwise",
            "our","ours","ourselves","out","over","own","package","pad","per","perhaps",
            "plain","please","pm","printable","private","protected","public","put",
            "quot","quote","r1","r2","rather","re","really","regards","reply","return",
            "right","said","same","sans","sat","say","saying","see","seem","seemed",
            "seeming","seems","serif","serious","several","she","short","should","show",
            "side","since","sincere","six","sixty","size","so","solid","some","somehow",
            "fuck", "shit", "...", "fucking", "yeah", "homosexuality", "fucker",
            "someone","something","sometime","sometimes","somewhere","span","src",
            "static","still","strictfp","string","strong","style","stylesheet","subject",
            "such","sun","super","sure","switch","synchronized","table","take","target",
            "td","text","th","than","thanks","that","the","their","them","themselves",
            "then","thence","there","thereafter","thereby","therefore","therein","thereupon",
            "these","they","thick","thin","think","third","this","those","though",
            "three","through","throughout","throw","throws","thru","thu","thus","tm",
            "to","together","too","top","toward","towards","tr","transfer","transient",
            "try","tue","type","ul","un","under","unsubscribe","until","up","upon",
            "us","use","used","uses","using","valign","verdana","very","via","void",
            "volatile","want","was","we","wed","weight","well","were","what","whatever",
            "when","whence","whenever","where","whereafter","whereas","whereby","wherein",
            "whereupon","wherever","whether","which","while","whither","who","whoever",
            "whole","whom","whose","why","width","will","with","within","without",
            "wont","would","wrote","www","yes","yet","you","your","yours","yourself",
            "yourselves", "lc", "il", "response", "method", "throughput", "developed",
            "allows", "system", "gy", "months", "quality", "given", "available",
            "low", "observed", "disease", "potential",
            "95", "art", "mm3", "df", "abat"

    );



    @Autowired
    private static SimpMessagingTemplate template;

    private static Connection connection;
    private static Session session;

    public static final List<Tweet> tweetList = new ArrayList<Tweet>();

    public static String read(String url) {
        StringBuilder buffer = new StringBuilder();
        try {
            // Kings Cross
            //https://api.twitter.com/1.1/geo/reverse_geocode.json?lat=51.5303&long=-0.1236
            // geocode:51.5303,-0.1236,5mi
            // The factory instance is re-useable and thread safe.
            Twitter twitter = TwitterFactory.getSingleton();
            // @Nature
            Query query = new Query("@BioMedCentral");
            QueryResult result = twitter.search(query);
            for (Status status : result.getTweets()) {
                System.out.println(status.getCreatedAt() + "@" + status.getUser().getScreenName() + ":" + status.getText());
                for (MediaEntity mediaEntity : status.getMediaEntities()) {
                    System.out.println("  --->" + mediaEntity.getMediaURL());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return buffer.toString();


    }




    private static List<String> wordList = new ArrayList<String>();

    private static final Comparator longComparator = new Comparator<Map.Entry<String, Long>>()
    {
        public int compare(Map.Entry<String, Long> o1, Map.Entry<String, Long> o2)
        {
            return o1.getValue().compareTo(o2.getValue());
        }
    };

    private static Map<String, Long> termFreqAll = new TreeMap(longComparator);


    public static void checkStream() throws TwitterException, IOException{

        StatusListener listener = new StatusListener(){
            public void onStatus(Status status) {

                System.out.println(status.getUser().getName() + " : " + status.getText());
//                String message = status.getUser().getName() + " : " + status.getText();
                String message = status.getText();

                if (message.startsWith("RT")) return;

                LatentSemanticAnalyzer ls = new LatentSemanticAnalyzer();
//                Set<String> stopWords = new HashSet<String>();
                List<String> newWordList = ls.plainTextToLemmas(message, stopWords, ls.createNLPPipeline());
                String sResult = newWordList.stream().map(Object::toString).collect(Collectors.joining(","));
//                List vv = termFreq.entrySet().stream().sorted((e1, e2) -> e1.getValue().compareTo(e2.getValue())).collect(Collectors.toList());

//                Arrays.asList(termFreq).stream().sorted().forEach(s -> System.out.println("-->>" + s));
                Map<String, Long> termFreq = wordList.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

                termFreqAll.entrySet().stream()
                        .forEach(entry -> termFreq.merge(entry.getKey(), entry.getValue(),
                                (listTwo, listOne) ->
                                {
                                    return listOne + listTwo;
                                }));

                List vv = termFreq.entrySet().stream().sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue())).collect(Collectors.toList());
                Arrays.asList(vv).stream().forEach(s -> System.out.println("-->>" + s));

                wordList = ListUtils.union(wordList, newWordList);

//                message = cleanText(message);
                twitterList.offer(message);
                twitterWordList.offer(sResult);
                System.out.println("-->>" + counter + ":" + message);
                Tweet tweet = new Tweet(message);
                tweetList.add(tweet);

                try {
                    TwitterConnect.writeToFile("/Users/davidhorby/data-mining/twitter/tweets" + new Date().getTime() + ".txt", tweet);
                } catch (IOException e) {
                    e.printStackTrace();
                }

//                if (++counter > MAX_TWEETS) {
//                    try {
//                        TwitterConnect.writeToFile("/Users/davidhorby/data-mining/twitter/tweets.txt", tweetList);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    System.exit(0);
//                }

//                wordListQueue.offer(topWords.toString());

//                twitterList.offer(message + "[[[" + sResult + "]]]");
            }

            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {}
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {}

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                System.out.println("onScrubGeo" + userId + ":" + upToStatusId);
            }

            @Override
            public void onStallWarning(StallWarning warning) {
                System.out.println("onStallWarning" + warning);
            }

            public void onException(Exception ex) {
                ex.printStackTrace();
            }
        };
        TwitterStream twitterStream = new TwitterStreamFactory().getInstance();
        twitterStream.addListener(listener);
        // sample() method internally creates a thread which manipulates TwitterStream and calls these adequate listener methods continuously.
        String[] language = new String[]{"en"};
//        String[] keywords = new String[] {"disease", "illness", "#ebola", "#flu",
//                "#influenza", "flu", "bird flu", "avian flu", "H5N1",
//                "meningitis", "hepatitis", "measles", "virus", "malaria"};
//        String[] keywords = new String[] {"bieber"};
//        String[] keywords = new String[] {"obama"};
        String[] keywords = new String[] {"sport","SPORT", "football", "tennis", "golf", "athletics"};
        FilterQuery fq = new FilterQuery();
        fq.language(language);
        fq.track(keywords);
        twitterStream.filter(fq);

    }

    public static void init() throws JMSException {
        // Create a ConnectionFactory
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

        // Create a Connection
        connection = connectionFactory.createConnection();
        connection.start();


    }

    public static void shutdown() throws JMSException {
        connection.close();
    }


        public static void postToActiveMQ(String messageText) {
            try {

                // Create a Session
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);


                // Create the destination (Topic or Queue)
                Destination destination = session.createQueue("twitter");


                // Create a MessageProducer from the Session to the Topic or Queue
                MessageProducer producer = session.createProducer(destination);
                producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

                TextMessage message = session.createTextMessage(messageText);

                // Tell the producer to send the message
                System.out.println("Sent message");
                producer.send(message);

                // Clean up
                session.close();
            }
            catch (JMSException e) {
                System.out.println("Caught: " + e);
                e.printStackTrace();

            }
        }


    public static void main(String[] args) {
        System.out.println("Started");
//        System.out.println(TwitterConnect.read("http://api.twitter.com/oauth/request_token"));
        try {
            init();
            TwitterConnect.checkStream();
//            shutdown();
        } catch (TwitterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JMSException e) {
            e.printStackTrace();
        }
        System.out.println("Finished");
    }

    public static void writeToFile(String fileName, List<Tweet> tweets) throws IOException {

        try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(fileName)), "UTF-8"))) {
             for (Tweet tweet : tweets) {
                out.write(tweet.getText());
                out.newLine();
             }
        }
    }

    public static void writeToFile(String fileName, Tweet tweet) throws IOException {

        try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(fileName)), "UTF-8"))) {
                out.write(tweet.getText());
                out.newLine();
        }
    }

}