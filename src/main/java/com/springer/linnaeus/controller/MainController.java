package com.springer.linnaeus.controller;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.*;


@RestController
public class MainController{


    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String redirect() {
        return "redirect:/index.html";
    }
}
