package com.springer.linnaeus.analyzer;

import com.springer.linnaeus.model.WordFreq;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.ling.CoreAnnotations.*;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.util.CoreMap;
import org.apache.commons.collections.ListUtils;
import scala.collection.mutable.ArrayBuffer;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class LatentSemanticAnalyzer {


    private Set<WordFreq> wordFreq = new HashSet<WordFreq>();


    public StanfordCoreNLP createNLPPipeline() {
        Properties props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma");
        return new StanfordCoreNLP(props);
    }



    public List<String> plainTextToLemmas(String text, List<String> stopWords, StanfordCoreNLP pipeline) {
        text = text.replaceAll("[\\.\\?!\\-#]","");
        text = text.replaceAll("[0-9]", "");
        Annotation doc = new Annotation(text);
        pipeline.annotate(doc);
        text = removeStopWords(text);
        List<String> lemmas = new ArrayList<String>();
        List<CoreMap> sentences = doc.get(SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {
            List<CoreLabel> tokens = sentence.get(TokensAnnotation.class);
            for (CoreLabel token : tokens) {
                String lemma = token.get(LemmaAnnotation.class);
                lemmas.add(lemma);
            }
        }
        List<String> uniqueLemmas = lemmas.stream()
                .distinct()
                .filter(s -> !isNumeric(s))
                .filter(s -> !s.startsWith("@"))
//                .filter(s -> !s.startsWith("#"))
                .filter(s -> !s.startsWith("http"))
                .filter(s -> !s.startsWith("-"))
                .filter(s -> s.length() > 2)
                .collect(Collectors.toList());
        uniqueLemmas = uniqueLemmas.stream().filter(word -> !stopWords.contains(word)).collect(Collectors.toList());
//        uniqueLemmas = ListUtils.union(uniqueLemmas, wordList);

        return uniqueLemmas;
    }

    public String removeStopWords (String text) {
        StringBuilder returnString = new StringBuilder();
        StringTokenizer st = new StringTokenizer(text);
        Arrays.stream(text.trim().split(" "))
                .map(this::evaluateMappings)
                .filter(this::evaluatePredicates)
                .forEach(returnString::append);
        return returnString.toString();

    }

    public static boolean isNumeric(String str) 	{
        return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
    }

    private String evaluateMappings(final String s) {
        String text = s;
        for (Function<String, String> f : getMappings()) {
            text = f.apply(text);
            if (s == null || s.isEmpty()) {
                return "";
            }
        }
        return text;
    }



    protected final Function<String, String> toTrimmedForm = s -> {
        return s.trim();
    };

    protected Function<String, String>[] getMappings() {
        return new Function[]{toTrimmedForm, toAlphanumeric};
    }

    protected final Function<String, String> toAlphanumeric = s -> {
        StringBuilder sb = new StringBuilder();
        for (char ch : s.toCharArray()) {
            if (Character.isLetterOrDigit(ch)) {
                sb.append(Character.toLowerCase(ch));
            }
        }
        return sb.toString();
    };

    private boolean evaluatePredicates(String s) {
        if (s == null || s.isEmpty()) {
            return false;
        }
        for (Predicate<String> p : getPredicates()) {
            if (!p.test(s)) {
                return false;
            }
        }
        return true;
    }




    protected Predicate<String>[] getPredicates() {
        return new Predicate[]{};
    }

}
